use crate::error_to_str;
use std::path::Path;
use crate::read_from_user;


pub fn init(directory: &Path) -> std::io::Result<()> {
    // TODO
    let directory = directory.join("pamp");
    println!("[INFO] Initializing your new pamp at {}", directory.as_os_str().to_str().ok_or(error_to_str("Failed to convert Path to string"))?);
    std::fs::create_dir_all(directory.clone())?;
    std::fs::create_dir_all(directory.join("pkg"))?;
    match std::fs::File::open(directory.join("config.toml")) {
        Err(_) => {
            let f = std::fs::File::create(directory.join("config.toml"))?;

        }
        Ok(f) => {
            println!("[WARN] You seem to be overwritting an existing pamp. Wish to OverWrite? [y/N]");
            let mut user_input = String::new();
            read_from_user(&mut user_input)?;

        }
    } 

    Ok(())
}
