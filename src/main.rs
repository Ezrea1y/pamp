use std::io;
use std::io::Read;

use clap::{Parser, Subcommand}; // For command line argument parsing (https://docs.rs/clap/latest/clap/index.html)
mod check;
mod init;
mod restore;

#[derive(Parser)]
#[command(
    about = "pamp: Automated Management of Packages\nSee https://gitlab.com/Ezrea1y/pamp for more documentation."
)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand, Clone)]
enum Commands {
    #[command(long_about = "Initialize a new pamp instance")]
    Init { directory: Option<String> },
    #[command(long_about = "Restores packages from a given pamp instance")]
    Restore { directory: Option<String> },
    #[command(long_about = "Checks if pamp folder is valid")]
    Check { directory: Option<String> },
}
fn main() -> io::Result<()> {
    let cli = Cli::parse();

    match cli.command {
        Commands::Init { directory } => init(directory)?,
        Commands::Restore { directory } => restore(directory)?,
        Commands::Check { directory } => check(directory)?,
    }
    Ok(())
}

fn init(directory: Option<String>) -> io::Result<()> {
    let path = get_directory(directory);
    init::init(&path)?;
    Ok(())
}

fn restore(directory: Option<String>) -> io::Result<()> {
    let path = get_directory(directory);
    restore::restore(&path)?;
    Ok(())
}

fn check(directory: Option<String>) -> io::Result<()> {
    let path = get_directory(directory);
    check::check(&path)?;
    Ok(())
}

fn get_directory(dir: Option<String>) -> std::path::PathBuf {
    match dir {
        Some(path) => {
            let path = std::path::PathBuf::from(path);
            if !path.exists() {
                panic!("Directory does not exist.")
            };
            path
        }

        None => {
            let current_directory = std::env::current_dir();
            match current_directory {
                Ok(path) => path,
                Err(error) => panic!("Problem getting the current directory: {:?}", error),
            }
        }
    }
}
pub fn error_to_str(s: &str) -> std::io::Error {
    std::io::Error::new(std::io::ErrorKind::Other, s)
}

pub fn read_from_user(s: &mut String) -> std::io::Result<usize> {
    Ok(std::io::stdin()
        .read_to_string(s)
        .map_err(|e| error_to_str(&e.to_string()))?)
}

pub fn ask_user_confirmation(s : &str) -> io::Result<bool> {
    println!("{s}");
    let mut user_input = String::new();
    loop {
        read_from_user(&mut user_input)?;
        match user_input.as_str() {
            "n" | "N" | "" => return Ok(false),
            "y" | "Y" => return Ok(true),
            _ => println!("[ERROR] Your input is not valid. Please enter only y or n."),
        }
    }
}
