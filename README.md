# pamp
pamp: automated management of packages.

> [!WARNING]
> Ideas still in progress. 
> Not indicative of final structure.


## Usage

1. Create a folder for pamp to manage all of your packages.

```bash
pamp init
```
A pamp folder will be created in the current directory with the following structure:

```
pamp/
├── pamp.toml
└── packages/
    ├── example/
    │   ├── example.toml
    │   └── # dot files, ...
    └── # other packages ...
```

2. Populate the `packages` folder.
   Each package should have a `package.toml` file and the necessary files to backup and restore
   the configuration, for example, dot files.

   ```toml
   title = "example"

   [installation]
   method = "pacman"

   [configuration]
   dots = "~/.config/example"
   ```

   Everything inside the folder (except the .toml file) will be copied to the specified directory.
   The available configuration options are:
    - `pacman`: for Arch Linux based distros.
    - `aur`: for AUR packages (installed using yay).
    - `git`: for git repositories (Necessary to specify the url and the script to compile).
    - `script`: for custom scripts (Necessary to specify the script to run).
    - ...

3. Populate the `pamp.toml` file with the packages you want to manage (in order of installation).

   ```toml
   [owner]
   name = "me"

   [packages]
   packages = [
         "example",
         # other packages ...
   ]
   ```

4. Execute pamp with superuser privileges.

   ```bash
    sudo pamp restore /directory/to/pamp/folder
   ```
